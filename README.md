###Answer 1: Count the number of employees for each level
###Answer 2: Create the function to merge the dataframe and count the number of people managed by each employee
###Answer 3: Preprocess the data and did exploratory analysis before fit the data to the models. 
###Scaled the data for multiple linear regression, ridge regression and lasso regression using minmaxscaler.
###This is because minmaxscaler is able to handle dummy variables without changing any values for dummy variables.
###After testing out on the standard scaler and minmaxscaler on mlr, I also found out that minmaxscaler perform
###better than standard scaler.
###. Use mean_squared_error to 
###determine which models perform the best
###Answer 4: Conduct variation plot to plot out the importance of variables among different models. Suggestion was written 
###at the end of the jupyter notebook.